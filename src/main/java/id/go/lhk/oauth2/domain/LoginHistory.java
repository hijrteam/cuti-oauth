package id.go.lhk.oauth2.domain;

public class LoginHistory extends Login {
	
	private String status;
	
	public LoginHistory() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public LoginHistory(String id) {
		// TODO Auto-generated constructor stub
		super(id);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
}
