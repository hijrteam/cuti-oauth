package id.go.lhk.oauth2.domain;

import java.util.Date;

public class LoginRequest {

	private String id;
	private String clientId;
	private String callback;
	
	public LoginRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}


	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}


}
