package id.go.lhk.oauth2.controller;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@Controller
public class PageController {
	
	@Value("${app.cookie.name}")
	private String cookieName;
	
//	@Value("${app.url.portal}")
//	private String portalUrl;

	@RequestMapping("/login")
    public String login(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		return loadIndex(model, request, response);
    }
	
	@RequestMapping("/")
    public String index(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		return loadIndex(model, request, response);
    }
	
	private String getCookieTokenValue(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();

		String accessTokenValue = "";
		if(cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				String name = cookies[i].getName();
				String value = cookies[i].getValue();
				// System.out.println(name + "--" + value);
				if (name.equals(cookieName)) {
					accessTokenValue = value;
				}
			}
		}

		return accessTokenValue;
	}
	
	private String loadIndex(Model model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String accessTokenValue = getCookieTokenValue(request);
		
//		if(request.isUserInRole("USER")) {
//			response.sendRedirect(portalUrl);
//			return "";
//		}
//		
		
		
			System.out.println(">> " + accessTokenValue);

		if (!accessTokenValue.equals("")) {
			String[] tokenArr = accessTokenValue.split("\\.");
			String payload = new String(Base64.getDecoder().decode(tokenArr[1]));
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = new HashMap<String, Object>();
			System.out.println(payload);
			// convert JSON string to Map
			map = mapper.readValue(payload, new TypeReference<Map<String, Object>>() {
			});

			// System.out.println(map.get("authorities"));
			model.addAttribute("realName", map.get("real_name").toString());
			model.addAttribute("userName", map.get("user_name").toString());
			model.addAttribute("position", map.get("position_name").toString());
			model.addAttribute("picture", map.get("user_picture").toString());
//			model.addAttribute("portalUrl", portalUrl);
		}

		return "index";
	}
}
