package id.go.lhk.oauth2;

public enum OrganizationReference {
	HIJR("HIJR","1508629134827")
    ;

	 private final String text;
	    private final String value;

	    /**
	     * @param text
	     */
	    private OrganizationReference(final String text, final String value) {
	        this.text = text;
	        this.value = value;
	    }
	    
	    public String value() {
	        return value;
	    }

	    /* (non-Javadoc)
	     * @see java.lang.Enum#toString()
	     */
	    @Override
	    public String toString() {
	        return text;
	    }
}
