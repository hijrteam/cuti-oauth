package id.go.lhk.oauth2.handler;

import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import id.go.lhk.oauth2.domain.Login;
import id.go.lhk.oauth2.domain.LoginHistory;
import id.go.lhk.oauth2.domain.OauthUserOrganization;
import id.go.lhk.oauth2.mapper.LoginMapper;

@Configuration
public class CustomLogoutHandler implements LogoutHandler {
	
	@Autowired
    private LoginMapper loginMapper;
	
	@Value("${app.cookie.name}")
	private String cookieName;

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		// TODO Auto-generated method stub
//		System.out.println(loginMapper + " == " + authentication);
//		System.out.println("cookie: " + cookieName);
		
		
		if(authentication != null) {
			Cookie cookie = new Cookie("JSESSIONID", null);
			String cookiePath = request.getContextPath();
			cookie.setPath(cookiePath);
			cookie.setMaxAge(0);
			response.addCookie(cookie);
			
			
			Cookie cookie1 = new Cookie(cookieName, null);
			cookie1.setPath("/");
			cookie1.setMaxAge(0);
			if (!request.getServerName().equals("localhost")) {
				cookie1.setDomain("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", ""));
			}
			response.addCookie(cookie1);
			
			OauthUserOrganization userOrg = (OauthUserOrganization) authentication.getPrincipal();
		
			List<Login> lst = loginMapper.findList(Login.USER_ID+"='"+userOrg.getId()+"'");
	        for(Login login: lst) {
	        		LoginHistory loginHistory = new LoginHistory(login.getId());
	            loginHistory.setAccessToken(login.getAccessToken());
	            loginHistory.setRefreshToken(login.getRefreshToken());
	            loginHistory.setClientId(login.getClientId());
	            loginHistory.setUserName(login.getUserName());
	            loginHistory.setCreatedTime(login.getCreatedTime());
	            loginHistory.setExpireTime(login.getExpireTime());
	            loginHistory.setTokenObject(login.getTokenObject());
	            loginHistory.setOrganizationId(login.getOrganizationId());
	            loginHistory.setUserId(login.getUserId());
	            loginHistory.setStatus("logout");
	            if(login.getExpireTime().before(new Date())) {
	            		loginHistory.setStatus("expired");
	            }
	            	System.out.println(loginHistory.getId() + " == "+ loginHistory.getStatus());
	            loginMapper.insertLoginHistory(loginHistory); // 1
	    			loginMapper.deleteActiveLogin(login); // 2
	        }
	        
		}
	}

}
