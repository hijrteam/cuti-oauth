package id.go.lhk.oauth2;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

public class Utils {

	private static final long NUM_100NS_INTERVALS_SINCE_UUID_EPOCH = 0x01b21dd213814000L;
	
	public static String getUUIDString() {
		NoArgGenerator timeBasedGenerator = Generators.timeBasedGenerator();

        //Generate time based UUID
        UUID firstUUID = timeBasedGenerator.generate();
		return firstUUID.toString();
	}
	
	public static Date getUUIDDate(String uuidString) {
		return new Date((UUID.fromString(uuidString).timestamp() - NUM_100NS_INTERVALS_SINCE_UUID_EPOCH) / 10000);
	}
	
	public static long getDateDiffFromNow(Date d1) {
		Date d2 = new Date();
        long seconds = (d2.getTime()-d1.getTime())/1000;
        return seconds;
	}
}
