package id.go.lhk.oauth2.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import id.go.lhk.oauth2.domain.OauthUserOrganization;
import id.go.lhk.oauth2.mapper.UserMapper;

@Configuration
public class CustomTokenEnhancer implements TokenEnhancer {
	
	@Value("${user.picture.url}")
    private String pictureUrl;
	
	@Autowired
    private UserMapper userMapper;
	
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>();
//        String clause =  OauthUserOrganization.ID + "='"+authentication.getName()+"'";
//        OauthUserOrganization user = userMapper.findListWithOrganization(clause).get(0);
        OauthUserOrganization user = (OauthUserOrganization) authentication.getPrincipal();
		System.out.println("prinsipal: " + authentication.getPrincipal().getClass());
		
        additionalInfo.put("organization_id", user.getOrganizationId());
        additionalInfo.put("organization_name", user.getOrganizationName());
        additionalInfo.put("position_id", user.getPositionId());
        additionalInfo.put("position_name", user.getPositionName());
        additionalInfo.put("user_id", user.getId());
        additionalInfo.put("real_name", user.getFirstName() + " " + user.getLastName());
        additionalInfo.put("user_picture", pictureUrl+user.getId());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }

}
