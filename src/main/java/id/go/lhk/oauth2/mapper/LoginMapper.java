package id.go.lhk.oauth2.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import id.go.lhk.oauth2.domain.Login;
import id.go.lhk.oauth2.domain.LoginHistory;
import id.go.lhk.oauth2.domain.LoginRequest;
import id.go.lhk.oauth2.domain.OauthClient;



@Mapper
public interface LoginMapper {
	
	@Insert("insert into tbl_login (id, access_token, client_id, user_name, refresh_token, created_time, expire_time, token_object, user_id, organization_id) values (#{id}, #{accessToken}, #{clientId}, #{userName}, #{refreshToken}, now(), #{expireTime}, #{tokenObject}, #{userId}, #{organizationId})")
	void insert(Login login);

	@Select("select * from tbl_login where access_token=#{accessToken} and expire_time > now()")
	Login findActiveLoginByAccessToken(@Param("accessToken") String accessToken);
	
	@Select("select * from tbl_login where ${clause}")
	List<Login> findList(@Param("clause") String clause);
	
	@Select("select * from tbl_login where refresh_token=#{refreshToken}")
	Login findLoginByRefreshToken(@Param("refreshToken") String refreshToken);
	
	@Select("select * from tbl_login where client_id=#{clientId} and user_name=#{userName} and expire_time > now()")
	Login findActiveLoginByClientAndUser(@Param("clientId") String clientId, @Param("userName") String userName);
	
	@Delete("delete from tbl_login where id=#{id}")
	void deleteActiveLogin(Login login);
	
	@Delete("delete from oauth_code where code=#{code}")
	void deleteOauthCode(@Param("code") String code);
	
	@Insert("insert into tbl_login_history (id, access_token, client_id, user_name, refresh_token, created_time, expire_time, token_object, status, user_id, organization_id) values (#{id}, #{accessToken}, #{clientId}, #{userName}, #{refreshToken}, #{createdTime}, #{expireTime}, #{tokenObject}, #{status}, #{userId}, #{organizationId})")
	void insertLoginHistory(LoginHistory loginHistory);
	
	@Select("select * from oauth_client_details where client_id=#{clientId} and client_secret=#{clientSecret}")
	OauthClient findOauthClient(@Param("clientId") String clientId, @Param("clientSecret") String clientSecret);
	
	@Select("select * from oauth_client_details where client_id=#{clientId}")
	OauthClient findOauthClientById(@Param("clientId") String clientId);
	
	@Insert("insert into tbl_login_request (id, client_id, callback, request_time) values (#{id}, #{clientId}, #{callback}, now())")
	void insertLoginRequest(LoginRequest loginRequest);
	
	@Select("select a.* from oauth_client_details a inner join tbl_login_request b where a.client_id=b.client_id and a.client_id=#{clientId} and b.id=#{requestId} and callback=#{callback}")
	OauthClient findOauthClientByLoginRequest(@Param("clientId") String clientId, @Param("requestId") String requestId, @Param("callback") String callback);
	
}
