package id.go.lhk.oauth2.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.go.lhk.oauth2.domain.OauthUser;
import id.go.lhk.oauth2.domain.OauthUserOrganization;



@Mapper
public interface UserMapper {
	
	public static final String QRY_USER_ORGANIZATION = "select a.*, c.position_id, name_position position_name, d.id organization_id, d.code organization_code, d.name organization_name from tbl_oauth_user a inner join tbl_oauth_user_position b on a.id=b.user_id inner join tbl_position c on b.position_id=c.position_id inner join tbl_organization d on b.organization_id=d.id";
	
	@Select("select * from ("+QRY_USER_ORGANIZATION+") as a where id=#{loginId} or username=#{loginId} or email=#{loginId} or mobile_phone=#{loginId}")
	OauthUser findUserByLoginId(@Param("loginId") String loginId);
	
	@Select("select * from tbl_oauth_user where ${clause}")
	List<OauthUser> findList(@Param("clause") String clause);
	
	@Select("select * from ("+QRY_USER_ORGANIZATION+") as a where ${clause}")
	List<OauthUserOrganization> findListWithOrganization(@Param("clause") String clause);
	
	@Select("select * from tbl_oauth_user where id=#{id}")
	OauthUser findUserById(@Param("id") String id);
	
	@Select("select b.name from tbl_oauth_user_role a inner join tbl_oauth_role b on a.role_id=b.id where a.user_id=#{userId}")
	List<String> findRolesByUserId(@Param("userId") String userId);
	
	@Select("select c.name from tbl_oauth_user_position a inner join tbl_oauth_position_role b on a.position_id=b.position_id inner join tbl_oauth_role c on role_id=c.id where user_id=#{userId} and organization_id=#{organizationId}")
	List<String> findRolesByUserOrganization(@Param("userId") String userId, @Param("organizationId") String organizationId);
	
	@Insert("insert into tbl_oauth_user_position (user_id, position_id, organization_id) values (#{userId}, #{positionId}, #{organizationId})")
	void insertUserPosition(@Param("userId") long userId, @Param("positionId") long positionId, @Param("organizationId") String organizationId);
	
	@Insert("insert into tbl_oauth_user_role select #{userId} user_id, role_id from tbl_oauth_position_role a inner join tbl_position b on a.position_id=b.position_id where a.position_id=#{positionId}")
	void insertUserRole(@Param("userId") long userId, @Param("positionId") long positionId);
	
	@Update("update tbl_oauth_user_position set position_id =#{positionId} where user_id=#{userId} and organization_id=#{organizationId}")
	void updateUserPosition(@Param("userId") long userId, @Param("positionId") long positionId, @Param("organizationId") String organizationId);
	
	@Delete("delete from tbl_oauth_user_position where user_id=#{userId} and organization_id=#{organizationId}")
	void deleteUserPosition(@Param("userId") long userId, @Param("organizationId") String organizationId);
	
	@Delete("delete from tbl_oauth_user_role where user_id=#{userId}")
	void deleteUserRole(@Param("userId") long userId);
	
	@Delete("delete from tbl_oauth_user where id=#{userId}")
	void deleteUser(@Param("userId") long userId);
	
	@Update("update tbl_oauth_user set password =#{newPassword} where id=#{userId}")
	void updateUserPassword(@Param("userId") String userId, @Param("newPassword") String newPassword);

}
