package id.go.lhk.oauth2.services;


import java.util.Arrays;

import javax.sql.DataSource;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.go.lhk.oauth2.Utils;
import id.go.lhk.oauth2.domain.Login;
import id.go.lhk.oauth2.domain.LoginHistory;
import id.go.lhk.oauth2.domain.OauthUserOrganization;
import id.go.lhk.oauth2.mapper.LoginMapper;
import id.go.lhk.oauth2.mapper.UserMapper;
import id.go.lhk.oauth2.security.CustomTokenEnhancer;


public class HijrTokenService extends DefaultTokenServices {
	
	@Autowired
    private UserMapper userMapper;
	
	@Autowired
    private LoginMapper loginMapper;
	
	
	@Override
	public OAuth2Authentication loadAuthentication(String accessTokenValue)
			throws AuthenticationException, InvalidTokenException {
		// TODO Auto-generated method stub
		System.out.println("loadAuthentication");
		OAuth2Authentication auth = super.loadAuthentication(accessTokenValue);
		return auth;
	}
	

	@Override
	public OAuth2AccessToken readAccessToken(String accessToken) {
		// TODO Auto-generated method stub
		System.out.println("readAccessToken");
		Login login = loginMapper.findActiveLoginByAccessToken(accessToken);
		if(login == null) {
			return null;
		}
		
		return super.readAccessToken(accessToken);
	}
		
	
	@Override
	public synchronized OAuth2AccessToken createAccessToken(OAuth2Authentication authentication)
	        throws AuthenticationException {
		System.out.println("createAccessToken");
		
		String clientId = authentication.getOAuth2Request().getClientId();
		String userName = authentication.getName();
		OauthUserOrganization userDetails = (OauthUserOrganization) authentication.getPrincipal();
//		System.out.println("prinsipal: " + authentication.getPrincipal().getClass());
		
//		OauthUser user = userMapper.findUserByLoginId(userId);
		Login login = loginMapper.findActiveLoginByClientAndUser(clientId, userDetails.getId());
		OAuth2AccessToken accessToken = null;
		
		if(login != null) {
			accessToken = (DefaultOAuth2AccessToken)SerializationUtils.deserialize(login.getTokenObject());
			
		}else {
			
			accessToken = super.createAccessToken(authentication);
			
			login = new Login();
			login.setId(Utils.getUUIDString());
			login.setAccessToken(accessToken.getValue());
			login.setRefreshToken(accessToken.getRefreshToken().getValue());
			login.setClientId(authentication.getOAuth2Request().getClientId());
			login.setUserName(userName);
			login.setExpireTime(accessToken.getExpiration());
			login.setTokenObject(SerializationUtils.serialize((DefaultOAuth2AccessToken)accessToken));
			login.setUserId(accessToken.getAdditionalInformation().get("user_id").toString());
			login.setOrganizationId(accessToken.getAdditionalInformation().get("organization_id").toString());
			
	    		loginMapper.insert(login);
		}
		
		
	    return accessToken;
	}

	
	@Override
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
	public OAuth2AccessToken refreshAccessToken(String refreshTokenValue, TokenRequest tokenRequest)
	        throws AuthenticationException {
		System.out.println("refreshAccessToken");
		OAuth2AccessToken accessToken = null;
		
		Login login = loginMapper.findLoginByRefreshToken(refreshTokenValue);
		if(login != null) {
			try {
				LoginHistory loginHistory = new LoginHistory(login.getId());
		        loginHistory.setAccessToken(login.getAccessToken());
		        loginHistory.setRefreshToken(login.getRefreshToken());
		        loginHistory.setClientId(login.getClientId());
		        loginHistory.setUserName(login.getUserName());
		        loginHistory.setCreatedTime(login.getCreatedTime());
		        loginHistory.setExpireTime(login.getExpireTime());
		        loginHistory.setTokenObject(login.getTokenObject());
		        login.setUserId(login.getUserId());
				login.setOrganizationId(login.getOrganizationId());
		        loginHistory.setStatus("refresh");
				loginMapper.insertLoginHistory(loginHistory); // 1
				loginMapper.deleteActiveLogin(login); // 2
				
				accessToken = super.refreshAccessToken(refreshTokenValue, tokenRequest);
				
				login.setId(Utils.getUUIDString());
				login.setAccessToken(accessToken.getValue());
				login.setRefreshToken(accessToken.getRefreshToken().getValue());
				login.setExpireTime(accessToken.getExpiration());
				login.setTokenObject(SerializationUtils.serialize((DefaultOAuth2AccessToken)accessToken));
				
				loginMapper.insert(login); // 3
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				throw new OAuth2Exception("internal server error");
			}
			
		}else {
			throw new OAuth2Exception("refresh token not found");
		}
		
		
		
		
		return accessToken;

	}

}