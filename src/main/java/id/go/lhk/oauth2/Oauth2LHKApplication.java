package id.go.lhk.oauth2;

import java.util.concurrent.Executor;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@SpringBootApplication
@EnableAsync
@MapperScan({"id.go.lhk.oauth2.mapper"})
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan
public class Oauth2LHKApplication extends SpringBootServletInitializer {

	@Autowired
    private DataSource dataSource;
	
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	   public DataSourceTransactionManager transactionManager() {
	       return new DataSourceTransactionManager(dataSource);
	   }
	
	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception {
	    SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
	    sessionFactory.setDataSource(dataSource);
//	    sessionFactory.setConfigLocation(new ClassPathResource("mybatis-config.xml"));
	    sessionFactory.getObject().getConfiguration().setMapUnderscoreToCamelCase(true);
	    sessionFactory.getObject().getConfiguration().setDefaultFetchSize(100);
	    sessionFactory.getObject().getConfiguration().setDefaultStatementTimeout(30);
	    return sessionFactory.getObject();
	}
	
	@Bean
    public InternalResourceViewResolver resolver() {
        InternalResourceViewResolver vr = new InternalResourceViewResolver();
        vr.setPrefix("/WEB-INF/jsp/");
        vr.setSuffix(".jsp");
        return vr;
    }
	
	@Bean
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("NotificationSender-");
        executor.initialize();
        return executor;
    }
	
	
	/**
	 * di pake untuk deploy jadi war
	 */
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Oauth2LHKApplication.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(Oauth2LHKApplication.class, args);
	}

}
