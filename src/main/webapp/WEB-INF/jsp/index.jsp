<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">

<head>
	<title>Single Sign On (SSO) - Kementerian Lingkungan Hidup dan Kehutanan</title>
	<link href="${pageContext.request.contextPath}/images/logo.png" rel="shortcut icon" type="image/x-icon" />
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
  	
  	
  	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style.css">

        

        <!-- Favicon and touch icons -->
        
  	
  	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
</head>
<body>
	 <!-- Top content -->
        <div class="top-content">
            <div class="inner-bg">
                <div class="container">                    
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4 col-md-5 col-md-offset-4 col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			
                        			<h4>Single Sign On (SSO)</h4>
                        			<c:choose>
						  <c:when test="${param.error != null}">
						    <p style="font-size: 20; color: #FF1C19;">Data yang anda masukkan salah, silahkan coba kembali.	</p>
						  </c:when>
						  <c:otherwise>
						    <p>Kementerian Lingkungan Hidup dan Kehutanan</p>
						  </c:otherwise>
						</c:choose>
                        			
                        		</div>
                        		<div class="form-top-right">
                        			<img id="logo" alt="hijr logo" src="${pageContext.request.contextPath}/images/logo.png" />
                        		</div>
                            </div>
                          <c:choose>
				  <c:when test="${param.success != null || pageContext.request.userPrincipal != null}">
					  <div class="form-bottom">
					  	<form class="login-form">
						    <p><img class="img-circle" src="${picture}" width="28px" /> <span style="font-family:arial; color:White; font-size: 18px;"><sec:authentication property="principal.firstName" />  <sec:authentication property="principal.lastName" /> </span> (<sec:authentication property="principal.positionName" />)
						   <br/>=> <a href="${portalUrl}">Masuk Portal Aplikasi</a> 
						     </p>
						    <button id="btnLogout" type="button" class="btn" onclick="location.href='logout'">Keluar</button>
						</form>
					</div>
				  </c:when>
				  <c:otherwise>
				    <div class="form-bottom">
			                    <form role="form" action="${pageContext.request.contextPath}/login" method="post" class="login-form" name="f">
			                    	<div class="form-group">
							  <c:if test="${realName == null}">
							    <label class="sr-only" for="form-username">Username</label>
			                        	<input autocomplete="off" type="text" name="username" placeholder="Username atau Email" class="form-username form-control" id="username">
							  </c:if>
			                    	<c:if test="${realName != null}">
							    <p><img class="img-circle" src="${picture}" width="28px" /> <span style="font-family:arial; color:White; font-size: 18px;">${realName} </span> (${position})
							    <input type="hidden" name="username" id="username" value="${userName}">
							  </c:if>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="password" placeholder="Password" class="form-password form-control" id="password">
			                        </div>
			                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			                        <div class="form-group">
			                        <div class="checkbox">
									  <label><input type="checkbox" name="remember-me"> <span style="color:White;">Tetap masuk</span></label>
									</div>
			                        </div>
			                        <c:choose>
			                        
							  <c:when test="${param.logout != null}">
							    <button id="btnLogin" type="submit" class="btn">Masuk Kembali</button>
							  </c:when>
							  <c:otherwise>
							    <button id="btnLogin" type="submit" class="btn">Masuk</button>
							  </c:otherwise>
							</c:choose>
			                        
			                      
			                      
			                      
			                        
			                    </form>
		                    </div>
				  </c:otherwise>
				</c:choose>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>

</body>
<script src="${pageContext.request.contextPath}/js/login-script.js"></script>

</html>